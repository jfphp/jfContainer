# jf/container

Contenedor de dependencias PSR-11.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/container` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/container
```

#### Dependencias

Cuando el proyecto es instalado, adicionalmente se instalan las siguientes dependencias:

| Paquete       | Versión |
|:--------------|:--------|
| jf/assert     | ^3.1    |
| psr/container | ^2.0    |

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone https://www.gitlab.com/jfphp/jfContainer.git
cd jfContainer
composer install
```

## Configuración

La configuración del contenedor es un mapa con el nombre de la intefaz como clave
y como valor la clase que la implementa. También es posible especificar como clave
un callable que se encargará de devolver la instancia requerida o devolver un
resultado que se usa como discriminador para seleccionar una clase u otra.

```php
function isLogged() : bool
{
    return true;
}

$container = Container::fromConfig([
    'classnames' => [
        IApp::class     => MyApp::class,     // <-- Se devuelve MyApp
        IDatbase::class => fn() => new Db(), // <-- Se ejecuta la función que devuelve Db.
        IUser::class    => [                 // <-- Interfaz a resolver
            'isLogged' => [                  // <-- Callable a ejecutar
                Guest::class: false,         // <-- `Guest` se devuelve si el resultado de `isLogged` es `false`.
                User::class:  true           // <-- `User` se devuelve si el resultado de `isLogged` es `true`.
            ],
            DefaulClass::class               // <-- Si isLogged no devolviera `false` o `true` se usaría esa clase.
        ]
    ]
]);
assert($container->get(IApp::class) instanceof MyApp);
assert($container[IDatabase::class] instanceof Db);   // Se puede usar notación de array
assert($container->get(IUser::class) instanceof User);
```

## Archivos disponibles

### Clases

| Nombre                                                          | Descripción                                                                                              |
|:----------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------|
| [jf\Container\Container](src/Container.php)                     | Contenedor para inyección de dependencias que implementa la interfaz `Psr\Container\ContainerInterface`. |
| [jf\Container\Exception\Container](src/Exception/Container.php) | Excepción genérica del contenedor.                                                                       |
| [jf\Container\Exception\NotFound](src/Exception/NotFound.php)   | Excepción lanzada cuando no se encuentra el identificador para obtener el valor del contenedor.          |
| [jf\Container\Exception\Type](src/Exception/Type.php)           | Excepción lanzada cuando se detecta algún valor incorrecto para el perámetro de un método.               |
| [jf\Container\Parameters](src/Parameters.php)                   | Construye los valores de los parámetros de una función o método.                                         |
| [jf\Container\Resolver\AResolver](src/Resolver/AResolver.php)   | Clase base para los resolutores de los identificadores usados en el contenedor.                          |
| [jf\Container\Resolver\Closure](src/Resolver/Closure.php)       | Resuelve la instancia usando reflexión.                                                                  |
| [jf\Container\Resolver\Interfaces](src/Resolver/Interfaces.php) | Construye una instancia determinando si es una interface y si la clase sugerida la implementa.           |
| [jf\Container\Resolver\Reflection](src/Resolver/Reflection.php) | Resuelve la instancia usando reflexión.                                                                  |
| [jf\Container\Resolver\Singleton](src/Resolver/Singleton.php)   | Verifica si la clase es un singleton provisto por el paquete `jf/base` y lo devuelve.                    |

### Interfaces

| Nombre                                                        | Descripción                                |
|:--------------------------------------------------------------|:-------------------------------------------|
| [jf\Container\Resolver\IResolver](src/Resolver/IResolver.php) | Interface para los resolutores de nombres. |
