<?php

namespace jf\Container;

use ArrayObject;
use jf\Base\ISingleton;
use jf\Container\Exception\Container as ContainerException;
use jf\Container\Exception\NotFound;
use jf\Container\Resolver\Closure;
use jf\Container\Resolver\Interfaces;
use jf\Container\Resolver\IResolver;
use jf\Container\Resolver\Reflection;
use jf\Container\Resolver\Singleton;
use Psr\Container\ContainerInterface;

/**
 * Contenedor para inyección de dependencias que implementa la interfaz `Psr\Container\ContainerInterface`.
 *
 * @section config Configuración
 *
 * La configuración del contenedor es un mapa con el nombre de la intefaz como clave
 * y como valor la clase que la implementa. También es posible especificar como clave
 * un callable que se encargará de devolver la instancia requerida o devolver un
 * resultado que se usa como discriminador para seleccionar una clase u otra.
 *
 * ```php
 * function isLogged() : bool
 * {
 *     return true;
 * }
 *
 * $container = Container::fromConfig([
 *     'classnames' => [
 *         IApp::class     => MyApp::class,     // <-- Se devuelve MyApp
 *         IDatbase::class => fn() => new Db(), // <-- Se ejecuta la función que devuelve Db.
 *         IUser::class    => [                 // <-- Interfaz a resolver
 *             'isLogged' => [                  // <-- Callable a ejecutar
 *                 Guest::class: false,         // <-- `Guest` se devuelve si el resultado de `isLogged` es `false`.
 *                 User::class:  true           // <-- `User` se devuelve si el resultado de `isLogged` es `true`.
 *             ],
 *             DefaulClass::class               // <-- Si isLogged no devolviera `false` o `true` se usaría esa clase.
 *         ]
 *     ]
 * ]);
 * assert($container->get(IApp::class) instanceof MyApp);
 * assert($container[IDatabase::class] instanceof Db);   // Se puede usar notación de array
 * assert($container->get(IUser::class) instanceof User);
 * ```
 *
 * @extends ArrayObject<string,callable|object>
 */
class Container extends ArrayObject implements ContainerInterface, ISingleton
{
    /**
     * Mapa de clases a usar.
     *
     * Permite relacionar interfaces con clases que las implementan pero en realidad se puede usar
     * también funciones y discriminadores.
     *
     * @var array
     */
    private array $_classnames = [];

    /**
     * Almacena la instancia usada como singleton.
     *
     * @var Container|null
     */
    private static ?Container $_instance = NULL;

    /**
     * Profundidad que lleva la recursividad tratando de resolver el identificador.
     *
     * @var int
     */
    private int $_depth = 0;

    /**
     * Lista de resolutores a aplicar.
     *
     * @var IResolver[]
     */
    private array $_resolvers = [];

    /**
     * Constructor de la clase.
     *
     * @param array<class-string,callable|object|string[]> $values Valores a usar para inicializar la instancia.
     */
    private function __construct(array $values = [])
    {
        self::$_instance = $this;
        // Agregamos los resolutores. El orden afecta el resultado.
        $values['resolvers'][] = Singleton::class;
        $values['resolvers'][] = Interfaces::class;
        $values['resolvers'][] = Closure::class;
        $values['resolvers'][] = Reflection::class;
        foreach ($values['resolvers'] as $resolver => $config)
        {
            if (is_int($resolver))
            {
                $resolver = $config;
                $config   = [];
            }
            if (is_string($resolver))
            {
                $resolver = new $resolver($this, ...($config ?? []));
            }
            $this->addResolver($resolver);
        }
        unset($values['resolvers']);
        // Autoregistramos la clase del contenedor.
        $values[ self::class ] = $this;
        // Si la clase del contenedor ha sido heredada la registramos la clase heredada.
        if (empty($values[ static::class ]))
        {
            $values[ static::class ] = $this;
        }
        if (empty($values[ ContainerInterface::class ]))
        {
            $values[ ContainerInterface::class ] = $this;
        }
        if (isset($values['classnames']))
        {
            $this->addClassnames($values['classnames']);
            unset($values['classnames']);
        }
        parent::__construct($values);
    }

    /**
     * Agrega un listado de clases que pueden ser instanciadas sin usar un closure.
     *
     * @param array<class-string,class-string> $classnames Nombres de clases que se agregarán.
     *                                                     La clave es la interfaz que implementan.
     *
     * @return static
     */
    public function addClassnames(array $classnames) : static
    {
        $this->_classnames = [ ...$this->_classnames, ...$classnames ];

        return $this;
    }

    /**
     * Agrega un resolutor de nombres.
     *
     * @param IResolver $resolver Resolutor a agregar.
     *
     * @return static
     */
    public function addResolver(IResolver $resolver) : static
    {
        if (!isset($this->_resolvers[ $resolver::class ]))
        {
            $this->_resolvers[ $resolver::class ] = $resolver;
        }

        return $this;
    }

    /**
     * Crea una instancia si no ha sido creada previamente y la inicializa con los valores especificados.
     *
     * @param array $config Configuración a usar para inicializar la instancia.
     *
     * @return static
     */
    public static function fromConfig(array $config = []) : static
    {
        return self::$_instance ?? new static($config);
    }

    /**
     * @inheritdoc
     */
    public function get(string $id) : mixed
    {
        ContainerException::lessOrEqual($this->_depth++, 64, _('La factoría para `{0}` es recursiva'), $id);
        $dep = $this->has($id)
            ? $this[ $id ]
            : NULL;
        if ($dep === NULL)
        {
            $hint = $this->_classnames[ $id ] ?? NULL;
            foreach ($this->_resolvers as $resolver)
            {
                $dep = $resolver->resolve($id, $hint);
                if ($dep !== NULL)
                {
                    break;
                }
            }
            $this[ $id ] = $dep;
        }
        // Evitamos los objetos que implementan __invoke__ ya que no podemos saber si es el objeto
        // que se quiere devolver o si es el responsable de devolver el valor a usar al invocarlo.
        elseif (!is_object($dep) && is_callable($dep))
        {
            $dep = ($this->_resolvers[ Closure::class ] ?? new Closure($this))->resolve($id, $dep);
        }
        NotFound::notIsNull($dep, _('No se pudo resolver {0}'), $id);
        --$this->_depth;

        return $dep;
    }

    /**
     * @inheritdoc
     */
    public function has(string $id) : bool
    {
        return isset($this[ $id ]);
    }

    /**
     * Devuelve la instancia usada como singleton.
     * Se recomienda llamar antes a `fromConfig` si se quiere personalizar la configuración.
     *
     * @return static
     */
    public static function i() : static
    {
        return self::$_instance ?? new static();
    }

    /**
     * @inheritdoc
     */
    public function offsetGet(mixed $key) : mixed
    {
        return $this->has($key)
            ? parent::offsetGet($key)
            : $this->get($key);
    }
}
