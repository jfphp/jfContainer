<?php

namespace jf\Container;

use ArrayObject;
use jf\Container\Exception\Type;
use Psr\Container\ContainerInterface;
use ReflectionFunctionAbstract;
use ReflectionNamedType;
use ReflectionParameter;
use Reflector;

/**
 * Construye los valores de los parámetros de una función o método.
 *
 * @extends ArrayObject<string,callable|object>
 */
readonly class Parameters
{
    /**
     * Constructor de la clase.
     *
     * @param ContainerInterface $container Contenedor de dependencias.
     */
    public function __construct(private ContainerInterface $container)
    {
    }

    /**
     * Construye el o los valores necesarios según el reflector especificado.
     *
     * @param Reflector $reflector Reflector a usar para obtener los parámetros.
     *
     * @return mixed
     */
    public function build(Reflector $reflector) : mixed
    {
        if ($reflector instanceof ReflectionFunctionAbstract)
        {
            $value = $this->buildParameters($reflector);
        }
        elseif ($reflector instanceof ReflectionParameter)
        {
            $value = $this->buildParameter($reflector);
        }
        else
        {
            $value = NULL;
        }

        return $value;
    }

    /**
     * Construye los argumentos para llamar la función o método a partir de
     * la configuración de los parámetros obtenidos por reflexión.
     *
     * @param ReflectionFunctionAbstract $reflector Reflector a usar para obtener los parámetros.
     *
     * @return array
     */
    private function buildParameters(ReflectionFunctionAbstract $reflector) : array
    {
        $params = [];
        foreach ($reflector->getParameters() as $parameter)
        {
            if ($parameter->isOptional())
            {
                break;
            }
            $params[] = $this->buildParameter($parameter);
        }

        return $params;
    }

    /**
     * Construye el valor del parámetro.
     *
     * @param ReflectionParameter $reflector Reflector del parámetro a analizar.
     *
     * @return mixed
     */
    private function buildParameter(ReflectionParameter $reflector) : mixed
    {
        if ($reflector->isOptional())
        {
            $value = NULL;
        }
        else
        {
            $type = Type::hasType($reflector);

            // Si lo acepta, usamos null sin inyectar alguna instancia.
            if ($type->allowsNull())
            {
                $value = NULL;
            }
            else
            {
                Type::notIsIntersection($reflector, $type);
                Type::notIsUnion($reflector, $type);
                Type::isNamed($reflector, $type);
                /** @var ReflectionNamedType $type */
                $value = $type->isBuiltin()
                    ? $this->container->get($reflector->getName())
                    : $this->container->get($type->getName());
            }
        }

        return $value;
    }
}
