<?php

namespace jf\Container\Exception;

use Psr\Container\NotFoundExceptionInterface;

/**
 * Excepción lanzada cuando no se encuentra el identificador
 * para obtener el valor del contenedor.
 */
class NotFound extends Container implements NotFoundExceptionInterface
{
}
