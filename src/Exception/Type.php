<?php

namespace jf\Container\Exception;

use jf\assert\Assert;
use Psr\Container\ContainerExceptionInterface;
use ReflectionIntersectionType;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionType;
use ReflectionUnionType;

/**
 * Excepción lanzada cuando se detecta algún valor incorrecto para el perámetro de un método.
 */
class Type extends Assert implements ContainerExceptionInterface
{
    /**
     * Verifica que el parámetro tenga un tipo asociado y lo devuelve.
     *
     * @param ReflectionParameter $parameter Parámetro a validar.
     *
     * @return ReflectionType
     */
    public static function hasType(ReflectionParameter $parameter) : ReflectionType
    {
        $type = $parameter->getType();
        if ($type === NULL)
        {
            throw new static(
                self::formatMessage(dgettext('container', '{0} necesita ser tipado'), self::parameterInfo($parameter))
            );
        }

        return $type;
    }

    /**
     * Verifica que el tipo del parámetro sea el apropiado.
     *
     * @param ReflectionParameter $parameter Parámetro a validar.
     * @param ReflectionType      $type      Tipo del parámetro.
     *
     * @return void
     */
    public static function isNamed(ReflectionParameter $parameter, ReflectionType $type) : void
    {
        if (!$type instanceof ReflectionNamedType && !$type->isBuiltin())
        {
            self::throwForWrongType($parameter, $type);
        }
    }

    /**
     * Verifica que el tipo del parámetro no sea una intersección ya que no se podría
     * determinar con precisión el valor apropiado.
     *
     * @param ReflectionParameter $parameter Parámetro a validar.
     * @param ReflectionType      $type      Tipo del parámetro.
     *
     * @return void
     */
    public static function notIsIntersection(ReflectionParameter $parameter, ReflectionType $type) : void
    {
        if ($type instanceof ReflectionIntersectionType)
        {
            self::throwForWrongType($parameter, $type);
        }
    }

    /**
     * Verifica que el tipo del parámetro no sea una unión ya que no se podría
     * determinar con precisión el valor apropiado.
     *
     * @param ReflectionParameter $parameter Parámetro a validar.
     * @param ReflectionType      $type      Tipo del parámetro.
     *
     * @return void
     */
    public static function notIsUnion(ReflectionParameter $parameter, ReflectionType $type) : void
    {
        if ($type instanceof ReflectionUnionType)
        {
            self::throwForWrongType($parameter, $type);
        }
    }

    /**
     * Construye la información del parámetro que se mostrará en el mensaje del error..
     *
     * @param ReflectionParameter $parameter Parámetro a analizar.
     *
     * @return string
     */
    private static function parameterInfo(ReflectionParameter $parameter) : string
    {
        $fn   = $parameter->getDeclaringFunction();
        $name = $fn->getShortName();
        if (!$fn->isClosure() && ($class = $parameter->getDeclaringClass()) !== NULL)
        {
            $name = explode("\0", $class->getName())[0] . '::' . $name;
        }

        return self::formatMessage(
            dgettext('container', 'Argumento {0} ({2}) de {4}()'),
            $parameter->getPosition() + 1,
            $parameter->getName(),
            $name
        );
    }

    /**
     * Lanza la excepción cuando el tipo del parámetro no es el apropiado.
     *
     * @param ReflectionParameter $parameter Parámetro a validar.
     * @param ReflectionType      $type      Tipo del parámetro.
     *
     * @return void
     */
    private static function throwForWrongType(ReflectionParameter $parameter, ReflectionType $type) : void
    {
        throw new static(
            self::formatMessage(
                dgettext('container', '{0} tiene un tipo `{2}` que no está soportado'),
                self::parameterInfo($parameter),
                (string) $type
            )
        );
    }
}
