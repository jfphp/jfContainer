<?php

namespace jf\Container\Exception;

use jf\assert\Assert;
use Psr\Container\ContainerExceptionInterface;

/**
 * Excepción genérica del contenedor.
 */
class Container extends Assert implements ContainerExceptionInterface
{
}
