<?php

namespace jf\Container\Resolver;

use jf\Base\ISingleton;

/**
 * Verifica si la clase es un singleton provisto por el paquete `jf/base` y lo devuelve.
 */
readonly class Singleton implements IResolver
{
    /**
     * Devuelve el singleton de la clase especificada o `null` si no implementa `jf\Base\ISingleton`.
     *
     * @param string $id Nombre de la clase.
     *
     * @return ISingleton|null
     */
    private function get(string $id) : ?object
    {
        return $id && class_exists($id) && is_a($id, ISingleton::class, TRUE)
            ? $id::i()
            : NULL;
    }

    /**
     * @inheritdoc
     */
    public function resolve(string $id, mixed $hint = NULL) : ?object
    {
        $item = NULL;
        if (interface_exists(ISingleton::class))
        {
            if (is_string($hint))
            {
                $item = $this->get($hint);
            }
            if (!$item)
            {
                $item = $this->get($id);
            }
        }

        return $item;
    }
}