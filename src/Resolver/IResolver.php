<?php

namespace jf\Container\Resolver;

/**
 * Interface para los resolutores de nombres.
 */
interface IResolver
{
    /**
     * Resuelve el FQCN que se debe usar para el identificador especificado.
     *
     * @param string     $id   Identificador que se quiere resolver.
     * @param mixed|null $hint Valor sugerido a usar para intentar resolver el identificador.
     *
     * @return object|null
     */
    public function resolve(string $id, mixed $hint = NULL) : ?object;
}
