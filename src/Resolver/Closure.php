<?php

namespace jf\Container\Resolver;

use jf\Container\Container;
use jf\Container\Exception\Container as ContainerException;
use jf\Container\Parameters;
use ReflectionFunction;

/**
 * Resuelve la instancia usando reflexión.
 *
 * Debería ser la última opción dado lo costoso de la reflexión y que lanzará
 * una excepción si no se encuentra la clase, interface o trait.
 */
readonly class Closure extends AResolver
{
    /**
     * Constructor de los parámetros de las funciones o métodos.
     *
     * @var Parameters
     */
    private Parameters $_parameters;

    /**
     * @inheritdoc
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->_parameters = new Parameters($container);
    }

    /**
     * @inheritdoc
     */
    public function resolve(string $id, mixed $hint = NULL) : ?object
    {
        $item = NULL;
        if ($hint && is_callable($hint))
        {
            // Construimos la lista de parámetros basados en su tipo
            $params = $this->_parameters->build(new ReflectionFunction($hint));
            // Invocamos la factoría con la lista de parámetros
            $item = $hint(...$params);
            if (is_string($item))
            {
                $item = $this->container($item);
            }
            ContainerException::isInstanceOf($item, $id, _('La factoría para `{2}` devolvió un valor inesperado `{0}`'));
        }

        return $item;
    }
}
