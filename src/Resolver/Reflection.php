<?php

namespace jf\Container\Resolver;

use jf\Container\Container;
use jf\Container\Exception\Container as ContainerException;
use jf\Container\Parameters;
use ReflectionClass;

/**
 * Resuelve la instancia usando reflexión.
 *
 * Debería ser la última opción dado lo costoso de la reflexión y que lanzará
 * una excepción si no se encuentra la clase, interface o trait.
 */
readonly class Reflection extends AResolver
{
    /**
     * Constructor de los parámetros de las funciones o métodos.
     *
     * @var Parameters
     */
    private Parameters $_parameters;

    /**
     * @inheritdoc
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->_parameters = new Parameters($container);
    }

    /**
     * @inheritdoc
     */
    public function resolve(string $id, mixed $hint = NULL) : ?object
    {
        ContainerException::isTrue(
            class_exists($id) || interface_exists($id) || trait_exists($id),
            _('Identificador `{0}` desconocido'),
            $id
        );
        $class = $hint && class_exists($hint)
            ? new ReflectionClass($hint)
            : new ReflectionClass($id);
        if (!$class->isInstantiable())
        {
            throw new ContainerException(
                ContainerException::formatMessage(
                    match (TRUE)
                    {
                        $class->isAbstract()  => _('No se puede instanciar la clase abstracta `{0}`'),
                        $class->isInterface() => _('No se puede instanciar la interfaz `{0}`'),
                        $class->isTrait()     => _('No se puede instanciar el trait `{0}`'),
                        default               => _('No se puede instanciar la clase `{0}`'),
                    },
                    $class->getName()
                )
            );
        }
        $ctor   = $class->getConstructor();
        $params = $ctor === NULL
            ? []
            : $this->_parameters->build($ctor);


        return $class->newInstance(...$params);
    }
}
