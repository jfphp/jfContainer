<?php

namespace jf\Container\Resolver;

use jf\Container\Container;

/**
 * Clase base para los resolutores de los identificadores usados en el contenedor.
 */
abstract readonly class AResolver implements IResolver
{
    /**
     * Contenedor de dependencias.
     *
     * @var Container
     */
    private Container $_container;

    /**
     * Constructor de la clase.
     *
     * @param Container $container Contenedor de dependencias.
     */
    public function __construct(Container $container)
    {
        $this->_container = $container;
    }

    /**
     * @see Container::get()
     */
    protected function container(string $id) : mixed
    {
        return $this->_container->get($id);
    }
}
