<?php

namespace jf\Container\Resolver;

/**
 * Construye una instancia determinando si es una interface y si la clase sugerida la implementa.
 */
readonly class Interfaces extends AResolver
{
    /**
     * @inheritdoc
     */
    public function resolve(string $id, mixed $hint = NULL) : ?object
    {
        return $id && $hint && is_string($hint) && interface_exists($id) && class_exists($hint) && is_a($hint, $id, TRUE)
            ? $this->container($hint)
            : NULL;
    }
}
