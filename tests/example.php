<?php

use jf\assert\Assert;
use jf\Base\ISingleton;
use jf\Container\Container;

require __DIR__ . '/../../autoload.php';

class A
{
}

class B extends A
{
    public function __construct(public readonly A $a)
    {
    }
}

class C
{
    public function __construct(public readonly B $b, public readonly int $int = 5)
    {
    }
}

class Str implements Stringable
{
    public function __toString() : string
    {
        return Str::class;
    }
}

class User implements Stringable
{
    public function __construct(private readonly string $username)
    {
    }

    public function __toString() : string
    {
        return $this->username;
    }
}

readonly class Json implements JsonSerializable
{
    public function __construct(public A $a)
    {
    }

    public function jsonSerialize() : string
    {
        return Json::class;
    }
}

class Singleton implements ISingleton
{
    private static ?Singleton $_instance = NULL;

    /**
     * @inheritDoc
     */
    static public function i() : static
    {
        return self::$_instance ?? (self::$_instance = new static());
    }
}

$container = Container::fromConfig(
    [
        'classnames' => [
            ISingleton::class => Singleton::class,
            Stringable::class => Str::class
        ]
    ]
);

$username                             = md5(random_bytes(16));
$container[ JsonSerializable::class ] = fn(A $a) => new Json($a);
$container['username']                = $username;

Assert::identical((string) $container->get(User::class), $username);
Assert::identical($container, Container::fromConfig());
Assert::identical($container, Container::i());
Assert::identical($container->get(ISingleton::class), Singleton::i());
Assert::identical($container->get(Singleton::class), Singleton::i());
Assert::identical($container[ A::class ], $container->get(B::class)->a);
Assert::isInstanceOf($container->get(C::class), C::class);
Assert::isInstanceOf($container->get(B::class), B::class);
Assert::isInstanceOf($container[ Stringable::class ], Str::class);
Assert::isInstanceOf($container->get(JsonSerializable::class), Json::class);
Assert::notIdentical($container->get(JsonSerializable::class), $container->get(JsonSerializable::class));
